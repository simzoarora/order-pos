import { Component, OnInit, Input } from '@angular/core';
// import items from './data/burger.json';
import { OrderServiceService } from '../order-service.service';
import { BehaviorSubject } from 'rxjs';
@Component({
  selector: 'app-explore-container',
  templateUrl: './explore-container.component.html',
  styleUrls: ['./explore-container.component.scss'],
})
export class ExploreContainerComponent implements OnInit {
  cart = [];
  products = [];
  cartItemCount: BehaviorSubject<number>;
  @Input() name: string;
  // public itemsList:{name:string, code:string}[] = items;

  constructor( public cartService: OrderServiceService,) { }

  ngOnInit() {
    this.products = this.cartService.getProducts();
    this.cart = this.cartService.getCart();
    this.cartItemCount = this.cartService.getCartItemCount();
  }
  doOrder(product){
    this.cartService.showOrder = true;
    this.cartService.addProduct(product);
    this.cartItemCount = this.cartService.getCartItemCount();
  }

}
