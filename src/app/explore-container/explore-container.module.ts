import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExploreContainerComponent } from './explore-container.component';
import {OrderWindowModule} from '../order-window/order-window.module'
// import { CartModalPageModule } from '../cart-modal/cart-modal.module'

@NgModule({
  imports: [ CommonModule, FormsModule, IonicModule, OrderWindowModule ],
  declarations: [ExploreContainerComponent],
  exports: [ExploreContainerComponent]
})
export class ExploreContainerComponentModule {}
