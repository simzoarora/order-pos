import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'pizza',
        loadChildren: () => import('../pizza/pizza.module').then(m => m.pizzaPageModule)
      },
      {
        path: 'burger',
        loadChildren: () => import('../burger/burger.module').then(m => m.Tab2PageModule)
      },
      {
        path: 'fries',
        loadChildren: () => import('../fries/fries.module').then(m => m.friesPageModule)
      },
      {
        path: 'bread',
        loadChildren: () => import('../bread/bread.module').then(m => m.Tab2PageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/pizza',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/pizza',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
