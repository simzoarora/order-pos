import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, AlertController } from '@ionic/angular';
import { Product, OrderServiceService } from '../order-service.service';


@Component({
  selector: 'app-ordersummary',
  templateUrl: 'ordersummary.page.html', 
  styleUrls: ['ordersummary.page.scss']
})

export class Tab2Page implements OnInit {
  cart: Product[] = [];

  constructor(private  cartService: OrderServiceService, private modalCtrl: ModalController, private alertCtrl: AlertController, private router: Router) { }

  ngOnInit() {
    this.cart = this.cartService.getCart();
  }
  getTotal() {
    return this.cart.reduce((i, j) => i + j.price * j.amount, 0);
  }
 
  close() {
    this.modalCtrl.dismiss();
  }
 
  async placeorder() {
    this.router.navigate(['/ordersummary'])
    console.log('hi');
  }

}
