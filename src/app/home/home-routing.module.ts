import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
    children: [
      {
        path: 'pizza',
        loadChildren: () => import('../pizza/pizza.module').then(m => m.pizzaPageModule)
      },
      {
        path: 'burger',
        loadChildren: () => import('../burger/burger.module').then(m => m.Tab2PageModule)
      },
      {
        path: 'fries',
        loadChildren: () => import('../fries/fries.module').then(m => m.friesPageModule)
      },
      {
        path: 'bread',
        loadChildren: () => import('../bread/bread.module').then(m => m.Tab2PageModule)
      },
      {
        path: 'chaap',
        loadChildren: () => import('../chaap/chaap.module').then(m => m.Tab2PageModule)
      },
      {
        path: 'wrap',
        loadChildren: () => import('../wrap/wrap.module').then(m => m.Tab2PageModule)
      },
      {
        path: 'sandwich',
        loadChildren: () => import('../sandwich/sandwich.module').then(m => m.Tab2PageModule)
      },
      {
        path: 'salad',
        loadChildren: () => import('../salad/salad.module').then(m => m.Tab2PageModule)
      },
      {
        path: 'ordersummary', 
        loadChildren: () => import('../ordersummary/ordersummary.module').then(m => m.Tab2PageModule)
      },
      {
        path: '',
        redirectTo: '../tabs/pizza',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule {}
