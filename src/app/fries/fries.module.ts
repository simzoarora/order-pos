import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { friesPage } from './fries.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { friesPageRoutingModule } from './fries-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    RouterModule.forChild([{ path: '', component: friesPage }]),
    friesPageRoutingModule,
  ],
  declarations: [friesPage]
})
export class friesPageModule {}
