import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { friesPage } from './fries.page';

describe('friesPage', () => {
  let component: friesPage;
  let fixture: ComponentFixture<friesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [friesPage],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(friesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
