import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { friesPage } from './fries.page';

const routes: Routes = [
  {
    path: '',
    component: friesPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class friesPageRoutingModule {}
