import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import {OrderWindowComponent} from './order-window.component'



@NgModule({
  imports: [
    CommonModule, FormsModule, IonicModule
  ],
  declarations: [OrderWindowComponent],
  exports: [OrderWindowComponent]
})
export class OrderWindowModule { }
