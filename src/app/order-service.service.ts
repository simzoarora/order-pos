import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
export interface Product {
  id: number;
  name: string;
  price: number;
  amount: number;
  image: string;
}
@Injectable({
  providedIn: 'root'
})
export class OrderServiceService {
  showOrder = false;
  constructor(public navCtrl: NavController, private router: Router) { }
  data: Product[] = [
    { id: 1, name: 'Veggie Burger', price: 49, image:  '../../assets/burger/veg_burger.png' , amount: 1 },
    { id: 2, name: 'Crispy Burger', price:69, image: '../../assets/burger/crispy_burger.png',  amount: 1  },
    { id: 3, name: 'Paneer Zinger Burger', price: 99, image:  '../../assets/burger/paneer_zinger_burger.png',  amount: 1 },
    { id: 4, name: 'Crispy Burger', price:69, image: '../../assets/burger/crispy_burger.png',  amount: 1  },
    { id: 5, name: 'Paneer Zinger Burger', price: 99, image:  '../../assets/burger/paneer_zinger_burger.png',  amount: 1 },
   
  ];
 
  private cart = [];
  private cartItemCount = new BehaviorSubject(0);
 
  getProducts() {
    return this.data;
  }
 
  getCart() {
    return this.cart;
  }
 
  getCartItemCount() {
    return this.cartItemCount;
  }
 
  addProduct(product) {
    let added = false;
    for (let p of this.cart) {
      if (p.id === product.id) {
        p.amount += 1;
        added = true;
        break;
      }
    }
    if (!added) {
      product.amount = 1;
      this.cart.push(product);
    }
    this.cartItemCount.next(this.cartItemCount.value + 1);
  }
 
  decreaseProduct(product) {
    for (let [index, p] of this.cart.entries()) {
      if (p.id === product.id) {
        p.amount -= 1;
        if (p.amount == 0) {
          this.cart.splice(index, 1);
        }
      }
    }
    this.cartItemCount.next(this.cartItemCount.value - 1);
  }
 
  removeProduct(product) {
    for (let [index, p] of this.cart.entries()) {
      if (p.id === product.id) {
        this.cartItemCount.next(this.cartItemCount.value - p.amount);
        this.cart.splice(index, 1);
      }
    }
  }

}
