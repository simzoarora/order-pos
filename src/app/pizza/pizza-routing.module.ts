import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { pizzaPage } from './pizza.page';

const routes: Routes = [
  {
    path: '',
    component: pizzaPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class pizzaPageRoutingModule {}
